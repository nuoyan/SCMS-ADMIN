package com.xmscltd.scms.base.web.base;

import com.jfinal.core.Action;
import io.jboot.web.controller.JbootController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 控制器基类
 * @author Rlax
 *
 */
public class BaseController extends JbootController {

    public BaseController(HttpServletRequest request, HttpServletResponse response) {

        request = request;
        response = response;

    }

    public BaseController() {
    }
}
