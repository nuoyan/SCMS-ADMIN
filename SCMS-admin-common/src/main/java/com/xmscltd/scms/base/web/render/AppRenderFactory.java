package com.xmscltd.scms.base.web.render;

import com.jfinal.render.ErrorRender;
import com.jfinal.render.JsonRender;
import com.jfinal.render.Render;
import com.xmscltd.scms.base.common.RestResult;
import io.jboot.web.render.JbootRenderFactory;

/**
 * RenderFactory，覆盖jboot error render
 * @author Rlax
 *
 */
public class AppRenderFactory extends JbootRenderFactory {

    private static final AppRenderFactory ME = new AppRenderFactory();



    @Override
    public Render getRender(String view) {
        return super.getRender(view);
    }

    @Override
    public Render getErrorRender(int errorCode) {
        return new ErrorRender(errorCode, constants.getErrorView(errorCode));
    }

    public Render getErrorJsonRender(int errorCode){
        Render render = null;
        switch (errorCode){
            case 403:
                render = new JsonRender(RestResult.buildError("很抱歉,您没有权限!"));
        }
        return render;
    }
}
