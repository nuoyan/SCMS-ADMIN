package xmscltd.job;

import com.jfinal.core.JFinal;
import io.jboot.Jboot;

/**
 * Starter
 * com.xuxueli.executor.sample.jfinal
 * Created with IntelliJ IDEA.
 * Description:
 * Author: alio-cmchen
 * Date: 2018-07-28
 * Time: 10:52
 * Version: V1.0.0
 */
public class Starter {

    public static void main(String[] args) {
        Jboot.run(args);
    }
}
