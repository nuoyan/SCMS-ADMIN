package xmscltd.job.executor.config;

import com.google.inject.Binder;
import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Routes;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.template.Engine;

import com.xxl.job.core.executor.XxlJobExecutor;
import io.jboot.Jboot;
import io.jboot.aop.jfinal.JfinalHandlers;
import io.jboot.aop.jfinal.JfinalPlugins;
import io.jboot.server.ContextListeners;
import io.jboot.server.JbootServer;
import io.jboot.server.Servlets;
import io.jboot.server.listener.JbootAppListenerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xmscltd.job.executor.jobhandler.DemoJobHandler;
import xmscltd.job.executor.jobhandler.ShardingJobHandler;

/**
 * JfinalConfigListener
 * com.xmscltd.scms.admin.config
 * Created with IntelliJ IDEA.
 * Description:
 * Author: Administrator-cmchen
 * Date: 2018-02-22
 * Time: 14:24
 * Version: V1.0.0
 */
public class JfinalConfigListener extends JbootAppListenerBase {

    private Logger logger = LoggerFactory.getLogger(JfinalConfigListener.class);
    // ---------------------- xxl-job executor ----------------------
    private XxlJobExecutor xxlJobExecutor = null;

    private void initXxlJobExecutor() {

        // registry jobhandler
        XxlJobExecutor.registJobHandler("demoJobHandler", new DemoJobHandler());
        XxlJobExecutor.registJobHandler("shardingJobHandler", new ShardingJobHandler());

        // load executor prop
        Prop xxlJobProp = PropKit.use("xxl-job-executor.properties");

        // init executor
        xxlJobExecutor = new XxlJobExecutor();
        xxlJobExecutor.setAdminAddresses(xxlJobProp.get("xxl.job.admin.addresses"));
        xxlJobExecutor.setAppName(xxlJobProp.get("xxl.job.executor.appname"));
        xxlJobExecutor.setIp(xxlJobProp.get("xxl.job.executor.ip"));
        xxlJobExecutor.setPort(xxlJobProp.getInt("xxl.job.executor.port"));
        xxlJobExecutor.setAccessToken(xxlJobProp.get("xxl.job.accessToken"));
        xxlJobExecutor.setLogPath(xxlJobProp.get("xxl.job.executor.logpath"));
        xxlJobExecutor.setLogRetentionDays(xxlJobProp.getInt("xxl.job.executor.logretentiondays"));

        // start executor
        try {
            xxlJobExecutor.start();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void destoryXxlJobExecutor() {
        if (xxlJobExecutor != null) {
            xxlJobExecutor.destroy();
        }
    }

    @Override
    public void onJfinalConstantConfig(Constants constants) {

    }

    @Override
    public void onJfinalRouteConfig(Routes routes) {
        routes.setBaseViewPath("/template");
    }

    @Override
    public void onJfinalEngineConfig(Engine engine) {

    }

    @Override
    public void onInterceptorConfig(Interceptors interceptors) {

    }

    @Override
    public void onJfinalPluginConfig(JfinalPlugins plugins) {

    }

    @Override
    public void onHandlerConfig(JfinalHandlers handlers) {
    }

    @Override
    public void onJFinalStarted() {

    }

    @Override
    public void onJFinalStop() {
        destoryXxlJobExecutor();

    }

    @Override
    public void onJbootStarted() {
        initXxlJobExecutor();
    }

    @Override
    public void onAppStartBefore(JbootServer jbootServer) {

    }

    @Override
    public void onJbootDeploy(Servlets servlets, ContextListeners listeners) {

    }

    @Override
    public void onGuiceConfigure(Binder binder) {

    }

}
