/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : scms

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-04-20 09:28:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department` (
  `id` int(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT '部门名称',
  `code` varchar(255) NOT NULL COMMENT '部门编码',
  `parentid` int(15) DEFAULT NULL COMMENT '父部门id',
  `parentname` varchar(255) DEFAULT NULL COMMENT '父部门名称',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建者',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(255) DEFAULT NULL COMMENT '更新者',
  `lastupdatetime` datetime DEFAULT NULL COMMENT '更新时间',
  `enable` varchar(1) DEFAULT '1' COMMENT '是否启用 1-启用 0-不启用',
  `LEAF` varchar(255) DEFAULT '0' COMMENT '叶子节点(0:树枝节点;1:叶子节点)',
  `REMARK` varchar(255) DEFAULT NULL COMMENT '备注',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES ('1', '研发部', 'sc!rd', '0', '司晨信息科技', null, null, null, null, '1', '0', null, null);
INSERT INTO `sys_department` VALUES ('2', '司晨信息科技', 'sc', null, null, null, null, null, null, '1', '0', null, null);
INSERT INTO `sys_department` VALUES ('3', '销售', 'sc!sa', '0', '司晨信息科技', null, null, null, null, '1', '0', null, null);
