package com.xmscltd.scms.qjob.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.xmscltd.scms.qjob.model.JobRegistry;

import java.util.List;

public interface JobRegistryService {

    /**
     * registryUpdate
     * @param registGroup
     * @param registryKey
     * @param registryValue
     */
    public int registryDelete(String registGroup, String registryKey, String registryValue);

    /**
     * registryUpdate
     * @param registGroup
     * @param registryKey
     * @param registryValue
     */
    public int registryUpdate(String registGroup, String registryKey, String registryValue);

    /**
     * registrySave
     * @param registGroup
     * @param registryKey
     * @param registryValue
     */
    public int registrySave(String registGroup, String registryKey, String registryValue);
    /**
     * find model by primary key
     *
     * @param timeout
     * @return
     */
    public void removeDead(int timeout);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public JobRegistry findById(Object id);


    /**
     * find all model
     *
     * @return all <Data
     */
    public List<JobRegistry> findAll();

    /**
     * find all model
     *
     * @return all <Data
     */
    public List<JobRegistry> findAllByTimeOut(int timeout);

    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(JobRegistry model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(JobRegistry model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(JobRegistry model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(JobRegistry model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
}