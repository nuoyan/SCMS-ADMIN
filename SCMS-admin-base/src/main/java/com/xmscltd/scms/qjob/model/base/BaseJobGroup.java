package com.xmscltd.scms.qjob.model.base;

import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.model.JbootModel;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseJobGroup<M extends BaseJobGroup<M>> extends JbootModel<M> implements IBean {

	// registry list
	private List<String> registryList;  // 执行器地址列表(系统注册)

	public List<String> getRegistryList() {
		String addressList = getAddressList();
		if (StringUtils.isNotBlank(addressList)) {
			registryList = new ArrayList<String>(Arrays.asList(addressList.split(",")));
		}
		return registryList;
	}

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public void setAppName(java.lang.String appName) {
		set("app_name", appName);
	}
	
	public java.lang.String getAppName() {
		return getStr("app_name");
	}

	public void setTitle(java.lang.String title) {
		set("title", title);
	}
	
	public java.lang.String getTitle() {
		return getStr("title");
	}

	public void setOrder(java.lang.Integer order) {
		set("order", order);
	}
	
	public java.lang.Integer getOrder() {
		return getInt("order");
	}

	public void setAddressType(java.lang.Integer addressType) {
		set("address_type", addressType);
	}
	
	public java.lang.Integer getAddressType() {
		return getInt("address_type");
	}

	public void setAddressList(java.lang.String addressList) {
		set("address_list", addressList);
	}
	
	public java.lang.String getAddressList() {
		return getStr("address_list");
	}

}
