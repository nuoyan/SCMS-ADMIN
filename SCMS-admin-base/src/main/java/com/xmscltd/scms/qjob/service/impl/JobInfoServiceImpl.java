package com.xmscltd.scms.qjob.service.impl;


import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.xmscltd.scms.qjob.model.JobInfo;
import com.xmscltd.scms.qjob.service.JobInfoService;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class JobInfoServiceImpl extends JbootServiceBase<JobInfo> implements JobInfoService {

    @Override
    public Long findAllCount() {
        String sql = Db.getSql("jobInfo.findAllCount");
        Long count = Db.queryLong(sql);
        return count;
    }

    @Override
    public Long triggerCountByHandleCode(int handleCode) {
        Kv cond = Kv.by("handleCode", handleCode);
        String sql = Db.getSqlPara("jobInfo.triggerCountByHandleCode",cond).getSql();
        Long count = Db.queryLong(sql);
        return count;
    }
}
