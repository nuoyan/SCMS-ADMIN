package com.xmscltd.scms.qjob.service.impl;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.xmscltd.scms.qjob.model.JobGroup;
import com.xmscltd.scms.qjob.service.JobGroupService;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;
import java.util.List;

@Bean
@Singleton
@JbootrpcService
public class JobGroupServiceImpl extends JbootServiceBase<JobGroup> implements JobGroupService {

    @Override
    public List<JobGroup> findByAddressType(int addressType) {
        Kv cond = Kv.by("addressType", addressType);
        SqlPara sql = Db.getSqlPara("jobGroup.findByAddressType",cond);
        return DAO.find(sql);
    }
}
