package com.xmscltd.scms.qjob.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.xmscltd.scms.qjob.model.JobLog;

import java.util.List;

public interface JobLogService {

    /**
     * triggerCountByDay
     * @param from yyyy-MM-dd
     * @param to yyyy-MM-dd
     * @return
     */
    public List<Record> triggerCountByDay(String from,
                                          String to);
    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public JobLog findById(Object id);


    /**
     * find all model
     *
     * @return all <Data
     */
    public List<JobLog> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(JobLog model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(JobLog model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(JobLog model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(JobLog model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
}