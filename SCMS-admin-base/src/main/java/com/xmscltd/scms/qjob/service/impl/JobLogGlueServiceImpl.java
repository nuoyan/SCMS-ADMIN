package com.xmscltd.scms.qjob.service.impl;


import com.xmscltd.scms.qjob.model.JobLog;
import com.xmscltd.scms.qjob.model.JobLogGlue;
import com.xmscltd.scms.qjob.service.JobLogGlueService;
import com.xmscltd.scms.qjob.service.JobLogService;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class JobLogGlueServiceImpl extends JbootServiceBase<JobLogGlue> implements JobLogGlueService {

}
