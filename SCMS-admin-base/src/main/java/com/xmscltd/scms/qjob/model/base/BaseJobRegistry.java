package com.xmscltd.scms.qjob.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseJobRegistry<M extends BaseJobRegistry<M>> extends JbootModel<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public void setRegistryGroup(java.lang.String registryGroup) {
		set("registry_group", registryGroup);
	}
	
	public java.lang.String getRegistryGroup() {
		return getStr("registry_group");
	}

	public void setRegistryKey(java.lang.String registryKey) {
		set("registry_key", registryKey);
	}
	
	public java.lang.String getRegistryKey() {
		return getStr("registry_key");
	}

	public void setRegistryValue(java.lang.String registryValue) {
		set("registry_value", registryValue);
	}
	
	public java.lang.String getRegistryValue() {
		return getStr("registry_value");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
