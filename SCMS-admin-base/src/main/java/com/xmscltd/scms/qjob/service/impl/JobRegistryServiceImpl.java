package com.xmscltd.scms.qjob.service.impl;


import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.xmscltd.scms.qjob.model.JobRegistry;
import com.xmscltd.scms.qjob.service.JobRegistryService;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;
import java.util.List;

@Bean
@Singleton
@JbootrpcService
public class JobRegistryServiceImpl extends JbootServiceBase<JobRegistry> implements JobRegistryService {

    @Override
    public void removeDead(int timeout) {
        Kv cond = Kv.by("timeout", timeout);
        SqlPara sql = Db.getSqlPara("jobRegistry.removeDead",cond);
        Db.delete(sql.getSql());
    }

    @Override
    public List<JobRegistry> findAllByTimeOut(int timeout) {
        Kv cond = Kv.by("timeout", timeout);
        SqlPara sql = Db.getSqlPara("jobRegistry.findAllByTimeOut",cond);
        return DAO.find(sql);
    }

    @Override
    public int registryUpdate(String registGroup, String registryKey, String registryValue) {
        Kv cond = Kv.by("registryGroup", registGroup).set("registryKey",registryKey).set("registryValue",registryValue);
        SqlPara sql = Db.getSqlPara("jobRegistry.registryUpdate",cond);
        return Db.update(sql);
    }

    @Override
    public int registrySave(String registGroup, String registryKey, String registryValue) {
        Kv cond = Kv.by("registryGroup", registGroup).set("registryKey",registryKey).set("registryValue",registryValue);
        SqlPara sql = Db.getSqlPara("jobRegistry.registrySave",cond);
        return Db.update(sql);
    }

    @Override
    public int registryDelete(String registGroup, String registryKey, String registryValue) {
        Kv cond = Kv.by("registryGroup", registGroup).set("registryKey",registryKey).set("registryValue",registryValue);
        String sql = Db.getSqlPara("jobRegistry.registrySave",cond).getSql();
        return Db.delete(sql);
    }
}
