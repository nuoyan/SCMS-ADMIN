package com.xmscltd.scms.qjob.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.xmscltd.scms.qjob.model.JobInfo;

import java.util.List;

public interface JobInfoService {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public JobInfo findById(Object id);

    /**
     * find All Count
     *
     * @return all <JobInfo>
     */
    public Long findAllCount();

    /**
     * find All Count
     *
     * @return all <JobInfo>
     */
    public Long triggerCountByHandleCode(int handleCode);


    /**
     * find all model
     *
     * @return all <JobInfo>
     */
    public List<JobInfo> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(JobInfo model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(JobInfo model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(JobInfo model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(JobInfo model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
}