package com.xmscltd.scms.qjob.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.xmscltd.scms.qjob.model.JobGroup;

import java.util.List;

public interface JobGroupService {

    /**
     * find model by addressType
     *
     * @param addressType
     * @return
     */
    public List<JobGroup> findByAddressType(int addressType);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public JobGroup findById(Object id);


    /**
     * find all model
     *
     * @return all <Data
     */
    public List<JobGroup> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(JobGroup model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(JobGroup model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(JobGroup model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(JobGroup model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
}