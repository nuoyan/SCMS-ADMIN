package com.xmscltd.scms.qjob.service.impl;


import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.xmscltd.scms.qjob.model.JobLog;
import com.xmscltd.scms.qjob.service.JobLogService;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;
import java.util.List;

@Bean
@Singleton
@JbootrpcService
public class JobLogServiceImpl extends JbootServiceBase<JobLog> implements JobLogService {

    @Override
    public List<Record> triggerCountByDay(String from, String to) {
        Kv cond = Kv.by("startDate", from).set("endDate",to);
        String sql = Db.getSqlPara("jobLog.triggerCountByDay",cond).getSql();
        List<Record> map = Db.find(sql);
        return map;
    }
}
