package com.xmscltd.scms.base.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.xmscltd.scms.base.model.Wxuser;

import java.util.List;

public interface WxuserService  {

    String getOpenidByUserId(Object userId);
    /**
     * find model userId
     *
     * @param userId
     * @return
     */
    public Wxuser findByUserId(Object userId);
    /**
     * find model by openId
     *
     * @param unionid
     * @return
     */
    public Wxuser findByUnionid(Object unionid);
    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Wxuser findById(Object id);


    /**
     * find all model
     *
     * @return all <Wxuser
     */
    public List<Wxuser> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Wxuser model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(Wxuser model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(Wxuser model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Wxuser model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
}