package com.xmscltd.scms.base.service.impl;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.xmscltd.scms.base.common.Consts;
import com.xmscltd.scms.base.common.ZTree;
import com.xmscltd.scms.base.model.Res;
import com.xmscltd.scms.base.model.User;
import com.xmscltd.scms.base.service.ResService;
import com.xmscltd.scms.base.service.UserService;
import com.xmscltd.scms.status.system.ResStatus;
import com.xmscltd.scms.status.system.RoleStatus;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Bean
@Singleton
@JbootrpcService
public class ResServiceImpl extends JbootServiceBase<Res> implements ResService {
	
	@Inject
	UserService userService;
	
    @Override
    public List<Res> findMenuByUserName(String name) {
    	User user = userService.findByUserName(name);
    	return findMenuByUserId(user);
    }

    @Override
    public Page<Res> findPage(String queryParm, int pageNumber, int pageSize, String whereStr) {

        Columns columns = Columns.create();
        if (StrKit.notBlank(queryParm)) {
            columns.like("name", "%"+queryParm+"%");
        }
        if (StrKit.notBlank(whereStr)) {
            columns.ne("id", whereStr);
        }
        columns.eq("status", 1);
        columns.eq("type", 1);
        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "id,pid,seq desc");
    }

    @Override
    public List<ZTree> findTreeOnUse() {
        Columns columns = Columns.create();
        columns.eq("status", ResStatus.USED);
        List<Res> list = DAO.findListByColumns(columns, "pid desc, seq asc");

        List<ZTree> zList = new ArrayList<ZTree>();
        for (Res res : list) {
            ZTree ztree = new ZTree(res.getId(), res.getName(), res.getPid());
            zList.add(ztree);
        }
        return zList;
    }

    @Override
    public List<ZTree> findAllTree() {
        List<Res> list = findAll();

        List<ZTree> zList = new ArrayList<ZTree>();
        for (Res res : list) {
            ZTree ztree = new ZTree(res.getId(), res.getName(), res.getPid());
            zList.add(ztree);
        }
        return zList;
    }

    @Override
    public List<ZTree> findTreeOnUseByRoleId(Long id) {
        List<ZTree> allTree = findTreeOnUse();
        List<Res> resList = findByRoleIdAndStatusUsed(id);

        List<Long> idList = new ArrayList<Long>();
        for (Res res : resList) {
            idList.add(res.getId());
        }

        for (ZTree tree : allTree) {
            if (idList.contains(tree.getId())) {
                tree.checked();
            }
        }
        return allTree;
    }

    @Override
    public List<Res> findByRoleIdAndStatusUsed(Long id) {
        SqlPara sp = Db.getSqlPara("system-res.findByRoleIdAndStatusUsed");
        sp.addPara(ResStatus.USED);
        sp.addPara(RoleStatus.USED);
        sp.addPara(id);
        return DAO.find(sp);
    }

    @Override
    public List<Res> findByStatus(String status) {
    	if(StrKit.isBlank(status)){
    		return DAO.findAll();
    	}else{
    		return DAO.findListByColumn("status", status);
    	}
    }

    @Override
    public List<Res> findByUserNameAndStatusUsed(String name) {
        SqlPara sp = Db.getSqlPara("system-res.findByUserNameAndStatusUsed");
        sp.addPara(ResStatus.USED);
        sp.addPara(RoleStatus.USED);
        sp.addPara(name);
        return DAO.find(sp);
    }

    @Override
    public List<Res> findTopMenuByUserName(String name) {
        SqlPara sp = Db.getSqlPara("system-res.findTopMenuByUserName");
        sp.addPara(ResStatus.USED);
        sp.addPara(RoleStatus.USED);
        sp.addPara(0L);
        sp.addPara(name);
        return DAO.find(sp);
    }

    @Override
    public List<Res> findLeftMenuByUserNameAndPid(String name, Long pid) {
        SqlPara sp = Db.getSqlPara("system-res.findLeftMenuByUserNameAndPid");
        sp.addPara(ResStatus.USED);
        sp.addPara(RoleStatus.USED);
        sp.addPara(pid);
        sp.addPara(name);
        return DAO.find(sp);
    }

    @Override
    public boolean hasChildRes(Long id) {
        return DAO.findFirstByColumn("pid", id) != null;
    }

	@Override
	public List<Res> findMenuByUserId(User user) {
		
        if(Consts.ROLE_SYSADMIN.equals(user.getRoleid())){
        	return DAO.findAll();
        }else{
    		SqlPara sp = Db.getSqlPara("system-res.findMenuByUserId");
    		sp.addPara(user.getId()); 
    		return DAO.find(sp);
        }

	}
}