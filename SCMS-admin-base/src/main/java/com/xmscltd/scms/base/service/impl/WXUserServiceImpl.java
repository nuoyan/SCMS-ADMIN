package com.xmscltd.scms.base.service.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.xmscltd.scms.base.model.Wxuser;
import com.xmscltd.scms.base.service.WxuserService;
import io.jboot.aop.annotation.Bean;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
public class WxuserServiceImpl extends JbootServiceBase<Wxuser> implements WxuserService {
    @Override
    public Wxuser findByUserId(Object userId) {
        return DAO.findFirstByColumn("userid",userId);
    }

    @Override
    public Wxuser findByUnionid(Object unionid) {
        return DAO.findFirstByColumn("unionid",unionid);
    }

    @Override
    public String getOpenidByUserId(Object userId) {
        SqlPara sp = Db.getSqlPara("system-WX.findWXByUserid");
        Record record = Db.findFirst(sp);
        return record.get("openid").toString();
    }
}