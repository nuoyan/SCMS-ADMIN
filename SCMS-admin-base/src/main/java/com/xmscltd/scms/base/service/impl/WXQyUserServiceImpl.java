package com.xmscltd.scms.base.service.impl;

import io.jboot.aop.annotation.Bean;
import com.xmscltd.scms.base.service.WxqyUserService;
import com.xmscltd.scms.base.model.WxqyUser;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
public class WxqyUserServiceImpl extends JbootServiceBase<WxqyUser> implements WxqyUserService {

}