package com.xmscltd.scms.base.service.impl;

import io.jboot.aop.annotation.Bean;
import com.xmscltd.scms.base.service.WxmaUserService;
import com.xmscltd.scms.base.model.WxmaUser;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
public class WxmaUserServiceImpl extends JbootServiceBase<WxmaUser> implements WxmaUserService {

}