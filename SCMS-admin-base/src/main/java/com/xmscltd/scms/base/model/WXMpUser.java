package com.xmscltd.scms.base.model;

import io.jboot.db.annotation.Table;
import com.xmscltd.scms.base.model.base.BaseWxmpUser;

/**
 * Generated by Jboot.
 */
@Table(tableName = "sys_wxmp_user", primaryKey = "id")
public class WxmpUser extends BaseWxmpUser<WxmpUser> {
	
}
