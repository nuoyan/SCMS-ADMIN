package com.xmscltd.scms.base.service.impl;

import io.jboot.aop.annotation.Bean;
import com.xmscltd.scms.base.service.WxmpUserService;
import com.xmscltd.scms.base.model.WxmpUser;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
public class WxmpUserServiceImpl extends JbootServiceBase<WxmpUser> implements WxmpUserService {

    @Override
    public WxmpUser findByUnionId(Object id) {
        return null;
    }
}