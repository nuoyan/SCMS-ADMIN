package com.xmscltd.scms.qjob.core.conf;


import io.jboot.config.annotation.PropertyConfig;

/**
 * xxl-job config
 *
 * @author xuxueli 2017-04-28
 */
@PropertyConfig(prefix = "xxl.job")
public class XxlJobAdminConfig {
    private static XxlJobAdminConfig adminConfig = null;
    public static XxlJobAdminConfig getAdminConfig() {
        return adminConfig;
    }

    public void afterPropertiesSet() throws Exception {
        adminConfig = this;
    }


    private String mailHost;

    private String mailPort;

    private boolean mailSSL;

    private String mailUsername;

    private String mailPassword;

    private String mailSendNick;

    private String loginUsername;

    private String loginPassword;

    private String i18n;

    private String config;

    private String accessToken;

    private String driverClass;

    private String dbUrl;

    private String dbUser;

    private String dbPassword;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getMailHost() {
        return mailHost;
    }

    public void setMailHost(String mailHost) {
        this.mailHost = mailHost;
    }

    public String getMailPort() {
        return mailPort;
    }

    public void setMailPort(String mailPort) {
        this.mailPort = mailPort;
    }

    public boolean isMailSSL() {
        return mailSSL;
    }

    public void setMailSSL(boolean mailSSL) {
        this.mailSSL = mailSSL;
    }

    public String getMailUsername() {
        return mailUsername;
    }

    public void setMailUsername(String mailUsername) {
        this.mailUsername = mailUsername;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public String getMailSendNick() {
        return mailSendNick;
    }

    public void setMailSendNick(String mailSendNick) {
        this.mailSendNick = mailSendNick;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getI18n() {
        return i18n;
    }

    public void setI18n(String i18n) {
        this.i18n = i18n;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }
}
