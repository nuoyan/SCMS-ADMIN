package com.xmscltd.scms.qjob.core.jobbean;

import com.xmscltd.scms.qjob.core.trigger.XxlJobTrigger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * http job bean
 * “@DisallowConcurrentExecution” diable concurrent, thread size can not be only one, better given more
 * @author xuxueli 2015-12-17 18:20:34
 */
//@DisallowConcurrentExecution
public class RemoteHttpJobBean implements Job {//extends QuartzJobBean {
	private static Logger logger = LoggerFactory.getLogger(RemoteHttpJobBean.class);

	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		// load jobId
		JobKey jobKey = jobExecutionContext.getTrigger().getJobKey();
		Integer jobId = Integer.valueOf(jobKey.getName());
		logger.info("==========================远程调用================");
		// trigger
		XxlJobTrigger.trigger(jobId);
	}
}