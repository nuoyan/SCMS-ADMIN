package com.xmscltd.scms.qjob.controller;

import cn.hutool.core.date.DateUtil;
import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.xmscltd.scms.base.web.base.BaseController;
import com.xmscltd.scms.qjob.model.JobGroup;
import com.xmscltd.scms.qjob.service.JobGroupService;
import com.xmscltd.scms.qjob.service.JobInfoService;
import com.xmscltd.scms.qjob.service.JobLogService;
import com.xxl.job.core.biz.model.ReturnT;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.*;

/**
 * QuartzController
 * com.xmscltd.scms.admin.controller.system
 * Created with IntelliJ IDEA.
 * Description:
 * Author: Administrator-cmchen
 * Date: 2018-03-02
 * Time: 15:39
 * Version: V1.0.0
 */
@RequestMapping("/qjob")
public class IndexController extends BaseController {

    @JbootrpcService
    private JobInfoService jobInfoService;

    @JbootrpcService
    private JobGroupService jobGroupService;

    @JbootrpcService
    private JobLogService jobLogService;

    public void index() {

        Map<String, Object> dashboardMap = getDashboard();
        setAttr("dashboard",dashboardMap);
        render("main.html");
    }

    /**
     * refresh count Dashboard
     */
    public void refreshDashboard(){
        Map<String, Object> dashboardMap = getDashboard();
        renderJson(dashboardMap);
    }

    /**
     * getEChartDashboard
     */
    public void getEChartDashboard(){
        Map<String, Object> eChartMap = getEChart();
        renderJson(eChartMap);
    }

    private Map<String, Object> getDashboard(){

        Long jobInfoCount = jobInfoService.findAllCount();
        Long jobLogCount = jobInfoService.triggerCountByHandleCode(-1);
        Long jobLogSuccessCount = jobInfoService.triggerCountByHandleCode(ReturnT.SUCCESS_CODE);

        // executor count
        Set<String> executerAddressSet = new HashSet<String>();
        List<JobGroup> groupList = jobGroupService.findAll();

        if (CollectionUtils.isNotEmpty(groupList)) {
            for (JobGroup group: groupList) {
                if (CollectionUtils.isNotEmpty(group.getRegistryList())) {
                    executerAddressSet.addAll(group.getRegistryList());
                }
            }
        }

        int executorCount = executerAddressSet.size();

        Map<String, Object> dashboardMap = new HashMap<String, Object>();
        dashboardMap.put("jobInfoCount", jobInfoCount);
        dashboardMap.put("jobLogCount", jobLogCount);
        dashboardMap.put("jobLogSuccessCount", jobLogSuccessCount);
        dashboardMap.put("executorCount", executorCount);
        return dashboardMap;
//        renderJson(dashboardMap);
    }

    private Map<String, Object> getEChart(){
        String startDate = getPara("startDate");
        String endDate = getPara("endDate");

        if (StrKit.isBlank(startDate)){
            startDate = DateUtil.formatDate(new Date());
        }
        if (StrKit.isBlank(endDate)){
            endDate = DateUtil.formatDate(DateUtil.tomorrow());
        }

        // process
        List<String> triggerDayList = new ArrayList<String>();
        List<Integer> triggerDayCountRunningList = new ArrayList<Integer>();
        List<Integer> triggerDayCountSucList = new ArrayList<Integer>();
        List<Integer> triggerDayCountFailList = new ArrayList<Integer>();
        int triggerCountRunningTotal = 0;
        int triggerCountSucTotal = 0;
        int triggerCountFailTotal = 0;

        List<Record> triggerCountMapAll = jobLogService.triggerCountByDay(startDate, endDate);
        if (CollectionUtils.isNotEmpty(triggerCountMapAll)) {
            for (Record item: triggerCountMapAll) {
                String day = item.getStr("triggerDay");
                int triggerDayCount = item.getInt("triggerDayCount");
                int triggerDayCountRunning = item.getInt("triggerDayCountRunning");
                int triggerDayCountSuc = item.getInt("triggerDayCountSuc");
                int triggerDayCountFail = triggerDayCount - triggerDayCountRunning - triggerDayCountSuc;

                triggerDayList.add(day);
                triggerDayCountRunningList.add(triggerDayCountRunning);
                triggerDayCountSucList.add(triggerDayCountSuc);
                triggerDayCountFailList.add(triggerDayCountFail);

                triggerCountRunningTotal += triggerDayCountRunning;
                triggerCountSucTotal += triggerDayCountSuc;
                triggerCountFailTotal += triggerDayCountFail;
            }
        } else {
            for (int i = 4; i > -1; i--) {
                triggerDayList.add(FastDateFormat.getInstance("yyyy-MM-dd").format(DateUtils.addDays(new Date(), -i)));
                triggerDayCountSucList.add(0);
                triggerDayCountFailList.add(0);
            }
        }

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("triggerDayList", triggerDayList);
        result.put("triggerDayCountRunningList", triggerDayCountRunningList);
        result.put("triggerDayCountSucList", triggerDayCountSucList);
        result.put("triggerDayCountFailList", triggerDayCountFailList);

        result.put("triggerCountRunningTotal", triggerCountRunningTotal);
        result.put("triggerCountSucTotal", triggerCountSucTotal);
        result.put("triggerCountFailTotal", triggerCountFailTotal);
        return result;
    }
}
