package com.xmscltd.scms.admin.controller.system;

import com.jfinal.core.paragetter.Para;
import com.jfinal.plugin.activerecord.Page;

import com.xmscltd.scms.base.common.RestResult;
import com.xmscltd.scms.base.model.Role;

import com.xmscltd.scms.base.rest.datatable.BootStrapDataTable;
import com.xmscltd.scms.base.service.RoleService;

import com.xmscltd.scms.base.web.base.BaseController;
import io.jboot.component.swagger.ParamType;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;


/**
 * UserController
 * com.xmscltd.scms.admin.controller.system
 * Created with IntelliJ IDEA.
 * Description:
 * Author: Administrator-cmchen
 * Date: 2018-03-14
 * Time: 11:27
 * Version: V1.0.0
 */
@RequestMapping("/system/role")
@Api(description = "角色管理相关", basePath = "/system/role", tags = "角色管理接口")
public class RoleController extends BaseController {

    @JbootrpcService
    private RoleService roleService;

    /**
     * 角色列表页面
     */
    public void index() {
        render("main.html");
    }

    /**
     * 获取角色列表
     */
    @ApiOperation(value = "获取角色列表", httpMethod = "GET", notes = "获取角色列表接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNumber", value = "页码", paramType = ParamType.PATH, dataType = "string", required = true),
            @ApiImplicitParam(name = "pageSize", value = "页内数据条数", paramType = ParamType.PATH, dataType = "string", required = true),
            @ApiImplicitParam(name = "sortName", value = "排序字段", paramType = ParamType.PATH, dataType = "string", required = false),
            @ApiImplicitParam(name = "sortOrder", value = "排序参数", paramType = ParamType.PATH, dataType = "string", required = false),
            @ApiImplicitParam(name = "searchText", value = "查询字段", paramType = ParamType.PATH, dataType = "string", required = false)
    })
    public void getRoleList() {
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String sortName = getPara("sortName");
        String sortOrder = getPara("sortOrder");
        String searchText = getPara("searchText");

        Page<Role> rolePage = roleService.findPage(pageNumber, pageSize, sortName, sortOrder, searchText);
        renderJson(new BootStrapDataTable<Role>(rolePage));
    }


    /**
     * 提交新增用户
     */
    @ApiOperation(value = "新增角色", httpMethod = "POST", notes = "表单新增角色接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Role", value = "Role对象", paramType = ParamType.FORM, dataType = "string", required = true),
    })
    public void addOrUpdate(@Para("") Role role) {

        RestResult<String> restResult = new RestResult<String>();

        if (role.getId() != null) {
            restResult.success().setMsg("更新成功");
            roleService.update(role);
        } else {
            restResult.success().setMsg("新增成功");
            roleService.save(role);
        }
        renderJson(restResult);
    }

    /**
     * 角色表格快速更新名字
     */
    @ApiOperation(value = "用户表格更新角色名", httpMethod = "POST", notes = "用户表格更新角色名接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pk", value = "角色ID", paramType = ParamType.FORM, dataType = "string", required = true),
            @ApiImplicitParam(name = "value", value = "角色名", paramType = ParamType.FORM, dataType = "string", required = true)
    })
    public void editName() {
        //String column = getPara("name");
        String id = getPara("pk");
        String newValue = getPara("value");

        Role role = roleService.findById(id);
        role.setName(newValue);
        roleService.update(role);

        renderNull();
    }

    /**
     * 角色表格快速更新备注
     */
    @ApiOperation(value = "角色表格更新备注", httpMethod = "POST", notes = "角色表格更新备注接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pk", value = "角色ID", paramType = ParamType.FORM, dataType = "string", required = true),
            @ApiImplicitParam(name = "value", value = "备注", paramType = ParamType.FORM, dataType = "string", required = true)
    })
    public void editDes() {
        String id = getPara("pk");
        String newValue = getPara("value");

        Role role = roleService.findById(id);
        role.setDes(newValue);
        roleService.update(role);

        renderNull();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除角色", httpMethod = "GET", notes = "删除角色接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "ID字符串 ex:1,2,3", paramType = ParamType.PATH, dataType = "string", required = true)
    })
    public void delete() {

        String ids = getPara("ids");
        String[] idsArr = ids.split(",");

        String str = "";
        //遍历删除，将删除成功的返回
        for (int i = 0; i < idsArr.length; i++) {
            if (roleService.deleteById(idsArr[i])) {
                str += idsArr[i] + ",";
            }
        }
        renderText(str);
    }

    /**
     * 校验角色名是否已被占用
     */
    @ApiOperation(value = "校验角色名是否重名", httpMethod = "POST", notes = "校验角色名重名接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "角色名", paramType = ParamType.QUERY, dataType = "string", required = true)
    })
    public void checkName() {
        String name = getPara("name");
        if (roleService.findByName(name) == null) {
            renderJson("valid", true);
        } else {
            renderJson("valid", false);
        }
    }

    /**
     * 根据id获取角色信息
     */
    @ApiOperation(value = "根据id获取角色信息", httpMethod = "GET", notes = "根据id获取角色信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色id", paramType = ParamType.PATH, dataType = "string", required = true)
    })
    public void getRoleById() {
        String id = getPara("id");
        Role role = roleService.findById(id);
        renderJson(role);
    }

    /**
     * 根据id获取角色信息
     */
    @ApiOperation(value = "根据用户id获取角色信息", httpMethod = "GET", notes = "根据用户id获取角色信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "用户id", paramType = ParamType.PATH, dataType = "string", required = true)
    })
    public void getRoleByUserId() {
        String id = getPara("userid");
        List<Role> role = roleService.findByUserName(id);
        renderJson(role);
    }
}
