var formatter = new Formatter();

function Formatter() {
    // Sample Format for edit Column.
    // =================================================================
    this.viewAuthFormatter = function (value, row, type) {

        var html = '<button onclick="authUtils.auth(' + row.id + ',\'Role\')" class="btn btn-info btn-labeled icon-lg fa fa-edit">查看授权</button>';
        return html;

    }


// Sample Format for RoleType Column.
// =================================================================
    this.roleTypeFormatter = function (value, row) {
        return '<span class="text-muted"><i class="fa fa-clock-o"></i> ' + value + '</span>';
    }

// Sample Format for edit Column.
// =================================================================
    this.roleEditFormatter = function (value, row) {

        var html = '<button onclick="edit(' + row.id + ')" class="btn btn-info btn-labeled icon-lg fa fa-edit">编辑</button>&nbsp;&nbsp;';
        html += '<button onclick="authUtils.auth(' + row.id + ',\'Role\')" class="btn btn-info btn-labeled icon-lg fa fa-check-square-o">授权</button>';
        return html;

    }

// Sample Format for icon Column.
// =================================================================
    this.roleIconFormatter = function (value, row) {

        return '<span class="text-muted"><i class="' + row.iconcls + '"></i></span>';
    }


// Sample Format for Order Status Column.
// =================================================================
    this.roleStatusFormatter = function (value, row) {
        var labelColor;
        if (value == "0") {
            labelColor = "success";
            value = "启用";
            /*    }else if(value == "Unpaid"){
                    labelColor = "warning";
                }else if(value == "Shipped"){
                    labelColor = "info";*/
        } else if (value == "1") {
            labelColor = "danger";
            value = "禁用";
        }
        return '<button onclick="enable(' + row.id + ',' + row.status + ')" class="label label-table label-' + labelColor + '"> ' + value + '</button>';
    }

    // Sample Format for Order Status Column.
// =================================================================
    this.userStatusFormatter = function (value, row) {
        var labelColor;
        if (value == "0") {
            labelColor = "success";
            value = "在职";
            /*    }else if(value == "Unpaid"){
                    labelColor = "warning";
                }else if(value == "Shipped"){
                    labelColor = "info";*/
        }else{
            labelColor = "danger";
            value = "离职";
        }
        return '<button onclick="enable(' + row.id + ',' + row.status + ')" class="label label-table label-' + labelColor + '"> ' + value + '</button>';

    }

    // =================================================================
    this.userEnableFormatter = function (value, row) {
        var labelColor;
        if (value == "0") {
            labelColor = "success";
            value = "正常";
            /*    }else if(value == "Unpaid"){
                    labelColor = "warning";
                }else if(value == "Shipped"){
                    labelColor = "info";*/
        }else{
            labelColor = "danger";
            value = "封号";
        }
        return '<button onclick="enable(' + row.id + ',' + row.enable + ')" class="label label-table label-' + labelColor + '"> ' + value + '</button>';

    }

    // Sample Format for edit Column.
// =================================================================
    this.userEditFormatter = function (value, row) {

        var html = '<button onclick="edit('+row.id+')" class="btn btn-info btn-labeled icon-lg fa fa-edit">编辑</button>&nbsp;&nbsp;'
        html += '<button onclick="authUtils.auth('+row.id+',\'User\')" class="btn btn-info btn-labeled icon-lg fa fa-check-square-o">授权</button>&nbsp;&nbsp;';
        html += '<button onclick="RoleSelect.init('+row.id+')" class="btn btn-info btn-labeled icon-lg fa fa-check-square-o">指定角色</button>';
        return html;
    }
}