/**
 * Ajax Utils
 * @type {Ajax}
 */
//全局对象
var ajax = new AjaxUtils();

function AjaxUtils() {

    this.REQUEST_TIMEOUT = 3 * 60 * 1000;
    this.REQUEST_TYPE = "POST";
    this.CONTENT_TYPE = "application/x-www-form-urlencoded; charset=UTF-8";
    this.REQUEST_ASYNC = true;
    this.REQUEST_CACHE = false;

    /**
     * 执行远程请求: 异步
     * @param url url
     * @param parameter 要提交的数据，是一个xml字符串。
     * @param _success 成功时的回调方法。
     * @param _error失败时的回调方法，可选。
     */
    this.call = function (url, parameter, _success, _error) {
        $.ajax({
            async: this.REQUEST_ASYNC,
            type: this.REQUEST_TYPE,
            cache: this.REQUEST_CACHE,
            contentType: this.CONTENT_TYPE,
            dataType: "json",
            timeout: this.REQUEST_TIMEOUT,
            url: url,
            data: parameter,
            success: function (data, textStatus, jqXHR) {
                if (_success != null && typeof(_success) != "undefiend") {
                    _success(data, textStatus);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                errorHandle(jqXHR, textStatus, errorThrown, _error);
            }
        });
    }

    /**
     * 执行远程请求: 同步
     * @param url url
     * @param parameter 要提交的数据，是一个xml字符串。
     * @param _success 成功时的回调方法。
     * @param _error失败时的回调方法，可选。
     */
    this.syncCall = function (url, parameter, _success, _error) {
        $.ajax({
            async: false,
            type: this.REQUEST_TYPE,
            cache: this.REQUEST_CACHE,
            contentType: this.CONTENT_TYPE,
            dataType: "json",
            timeout: this.REQUEST_TIMEOUT,
            url: url,
            data: parameter,
            success: function (data, textStatus, jqXHR) {
                if (_success != null && typeof(_success) != "undefiend") {
                    _success(data, textStatus);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                errorHandle(jqXHR, textStatus, errorThrown, _error);
            }
        });
    };


    /**
     * 全局异常处理
     **/
    var errorHandle = function (jqXHR, textStatus, errorThrown, _error) {
        if (jqXHR.status == '999') {
            bootbox.alert("由于长时间未操作，空闲会话已超时；您将被强制重定向到登录页面！", function () {
                // window.location = this.WEBCONTEXT + "login.do?reqCode=init";
            });
        } else if (jqXHR.status == '998') {
            bootbox.alert("您的会话连接由于在其它窗口上被注销而失效，系统将把您强制重定向到登录界面！", function () {
                // window.location = this.WEBCONTEXT + "login.do?reqCode=init";
            });
        } else if (jqXHR.status == '-1') {
            bootbox.alert("请求失败，超时或服务器无响应！");
        } else {
            bootbox.alert(jqXHR.responseText);
        }

        if (_error != null && typeof(_error) != "undefiend") {
            _error(textStatus, errorThrown);
        }
    }

    /**
     * 跨域执行远程请求。
     * sid: 服务SID，类似于XXXX:XXX的形式。
     * data: 要提交的数据，是一个xml字符串。
     * _success: 成功时的回调方法。
     * _error: 失败时的回调方法，可选。
     */
    this.crossDomainCall = function (url, parameter, _success, _error) {
        var requestUrl = "http://121.14.145.7:9080/Trust/" + url;
        $.ajax({
            async: this.REQUEST_ASYNC,
            type: this.REQUEST_TYPE,
            cache: this.REQUEST_CACHE,
            contentType: this.CONTENT_TYPE,
            dataType: "jsonp",
            timeout: this.REQUEST_TIMEOUT,
            url: requestUrl,
            data: parameter,
            jsonp: "jsonpCallback",
            success: function (data, textStatus) {
                if (_success != null && typeof(_success) != "undefiend") {
                    _success(data, textStatus);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                errorHandle(jqXHR, textStatus, errorThrown, _error);
            }
        });
    }

    /**
     * 执行远程请求。
     * url :请求路径
     * parameter ：参数类型
     * _success: 成功时的回调方法。
     */
    this.doAjaxPost = function (url, parameter, _success) {
        $.post(
            url,
            parameter,
            function (result, status) {
                result = $.parseJSON(result);
                _success(result, status);
            }
        );
    }
}


