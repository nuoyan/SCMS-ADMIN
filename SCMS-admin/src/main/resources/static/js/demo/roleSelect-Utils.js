/**
*  角色选择
*  author： sky_lv
**/
var RoleSelect = window.RoleSelect || {};

RoleSelect.init = function (userid, success) {
	//指定角色的用户ID
	this.userid = userid;
	//指定角色成功后的操作
	this.success = success;

	var initRoleTable = function() {

        $('#role-select').bootstrapTable({
            idField: 'id',
            uniqueId:'id',
            sortable:true,
            sortOrder: "desc",
            pagination: true,//是否显示分页（*）
            queryParamsType:'', //默认值为 'limit' ,在默认情况下 传给服务端的参数为：offset,limit,sort
                                // 设置为 ''  在这种情况下传给服务器的参数为：pageSize,pageNumber
            sidePagination: "server",   //分页方式：client客户端分页，server服务端分页（*）
            url: '/system/role/getRoleList',
            columns: [{
                checkbox: true
            },{
                field: 'id',
                /*formatter:'invoiceFormatter',*/
                title: 'ID',
                align: 'center',
                sortable:true
            }, {
                field: 'name',
                title: '角色名称',
                align: 'center',
                sortable:true
            }, {
                field: 'status',
                align: 'center',
                title: '状态',
                // formatter:'statusFormatter',
                sortable:true
            }, {
                field: 'des',
                title: '角色简介',
                align: 'center',
            },{
                field: '',
                title: '操作',
                align: 'center',
                formatter:'formatter.viewAuthFormatter',
            }]
        });

        $("#demo-default-modal").modal('show');
    }

	var initcheck = function (id){

        $('#role-select').bootstrapTable('uncheckAll');

        ajax.call("/system/role/getRoleByUserId", {userid : RoleSelect.userid},
            function(datas, status) {
        	var arr = null;
				 datas.forEach(function (index,data) {
                     arr.put(data.id);
                 })
                $('#role-select').bootstrapTable('checkBy', {field:"id", values:arr});
            }
        );

	}
	
	var submitrole = function(){
		$('#role-config').modal('hide');
		var roleid = '';
		$('#role-config').find("input[name='roleId']").each(function () {
	        if(jQuery(this).is(":checked")){
	        	roleid +=this.value+ ",";
	        }
	    });
		if(roleid.length > 0){
			roleid = roleid.substring(0, roleid.length-1);
		}
		
		ajax.call("omsUser.do?reqCode=saveSelectedRole", {userid : RoleSelect.userid, roleid : roleid},
			function(datas, status) {
				if (datas.success) {
					$.Prompt("角色保存成功！", 1000);
					if (typeof(success) == "function") {
						success();
					}
				}else{
					var msg = '<span>' + datas.msg + '</span>';
					bootbox.alert(msg);
				}
			}
		);
	}
	
	$("#role-config .roleSubmit").on("click", function() {
		submitrole();
	});
	
	initRoleTable();
}


/**
 * 全选 全不选
 * @param obj
 */
RoleSelect.roleCheckAll = function(obj) {
	if(obj.checked) {
		$('#role-config').find("input[name='roleId']").each(function(){
			$(this).attr("checked", true);
			$(this).parent().addClass("checked");
		});
	} else {
		$('#role-config').find("input[name='roleId']").each(function(){
			$(this).attr("checked", false);
			$(this).parent().removeClass();
		});
	}
}

RoleSelect.rolecheck = function(e){
   var checked = jQuery(e).is(":checked");
   if (checked) {
   		$(e).attr("checked", true);
    	$(e.parentNode).addClass("checked");
   }else{
    	$(e).attr("checked", false);
    	$(e.parentNode).removeClass();
    }
}
