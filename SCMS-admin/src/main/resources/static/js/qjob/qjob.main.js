
var lineOption;
var pieOption;
//init from to data 
var from = moment().format("YYYY-MM-DD");
var to = moment().add(1, 'days').format("YYYY-MM-DD");

$(document).ready(function () {

    var lineECharts = echarts.init(document.getElementById('lineECharts'));
    var pieECharts = echarts.init(document.getElementById('pieECharts'));

    //resize echarts when resize windows
    $(window).resize(function() {
        lineECharts.resize();
        pieECharts.resize();
    });

    // BOOTSTRAP DATEPICKER WITH RANGE SELECTION
    // =================================================================
    // Require Bootstrap Datepicker
    // http://eternicode.github.io/bootstrap-datepicker/
    // =================================================================
    $('#demo-dp-range .input-daterange').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        autoclose: true,
        language: "zh-CN",
        todayHighlight: true
        
    });

    //refresh echarts
    $('.echart-panel-ref-btn').niftyOverlay().on('click', function(){
        // Do refresh
        refreshECharts(from,to);
    });

    //refresh job log count
    $('.job-panel-ref-btn').niftyOverlay().on('click', function(){
        var $el = $(this), relTime;
        $el.niftyOverlay('show');

        // Do refresh

        refreshDashboard();

        relTime = setInterval(function(){
            // Hide the screen overlay
            $el.niftyOverlay('hide');

            clearInterval(relTime);
        },2000);
    });
    
    //refreshECharts
	function refreshECharts(from,to){
    	
        //refresh echarts

        var $el = $('.echart-panel-ref-btn'), relTime;
        $el.niftyOverlay('show');

        // Do refresh
        getOptionData(from,to);

        relTime = setInterval(function(){
                // Hide the screen overlay
        		$el.niftyOverlay('hide');

        		clearInterval(relTime);
            },2000);
	}

    getOptionData(from,to);
    
    //get option data form service
    function getOptionData(from,to) {

        $.ajax({
            url: "/qjob/getEChartDashboard",
            data:{
                "startDate":from,
                "endDate":to
            },
            success: function (result) {
                buildLineOption(result);
                buildPieOption(result);
                lineECharts.setOption(lineOption);
                pieECharts.setOption(pieOption);
            },
            error: function (result) {
                bootbox.alert({
                    closeButton: false,
                    message: result.responseJSON.msg,
                });
            }
        });
    }
    //build Line echarts Option
    function buildLineOption(result) {

        lineOption = {
            title: {
                text: "日期分布图",
            },
            tooltip : {
                trigger: 'axis',
               /* axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }*/
            },
            legend: {
                data:["成功","失败","运行中"]
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : result.triggerDayList
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:"成功",
                    type:'line',
                    // stack: 'Total',
                    // areaStyle: {normal: {}},
                    data: result.triggerDayCountSucList
                },
                {
                    name:"失败",
                    type:'line',
                    // stack: 'Total',
                    label: {
                        normal: {
                            show: true,
                            position: 'top'
                        }
                    },
                    // areaStyle: {normal: {}},
                    data: result.triggerDayCountFailList
                },
                {
                    name:"运行中",
                    type:'line',
                    // stack: 'Total',
                    // areaStyle: {normal: {}},
                    data: result.triggerDayCountRunningList
                }
            ],
            // color:['#00A65A', '#c23632', '#F39C12']
        };

    }
    //build pie echarts Option
    function buildPieOption(result) {
        pieOption = {
            title : {
                text: "成功比例图" ,
                /*subtext: 'subtext',*/
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ["成功","失败","运行中" ]
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            series : [
                {
                    //name: '分布比例',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {
                            name:"成功",
                            value:result.triggerCountSucTotal
                        },
                        {
                            name:"失败",
                            value:result.triggerCountFailTotal
                        },
                        {
                            name:"运行中",
                            value:result.triggerCountRunningTotal
                        }
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ],
            color:['#00A65A', '#c23632', '#F39C12']
        };
    }
    //refreshDashboard
    function refreshDashboard() {

        $.ajax({
            url: "/qjob/refreshDashboard",
            success: function (result) {
                $("#jobInfoCount").text(result.jobInfoCount);
                $("#jobLogCount").text(result.jobLogCount);
                $("#executorCount").text(result.executorCount);
            },
            error: function (result) {
                bootbox.alert({
                    closeButton: false,
                    message: result.responseJSON.msg,
                });
            }
        });
    }
    //qiuck shoose data
    filterDate = function(e) {
        var select = $(e).attr("id");
        if(select == "today"){
            from = moment().format("YYYY-MM-DD");
            to = moment().add(1, 'days').format("YYYY-MM-DD");
        }else if(select == "yesterday"){
            from = moment().subtract(1, 'days').format("YYYY-MM-DD");
            to = moment().format("YYYY-MM-DD");
        }else if(select == "this_month"){
            from = moment().startOf('month').format("YYYY-MM-DD");
            to = moment().endOf('month').format("YYYY-MM-DD");
        }else if(select == "last_month"){
            from = moment().subtract(1, 'months').startOf('month').format("YYYY-MM-DD");
            to = moment().subtract(1, 'months').endOf('month').format("YYYY-MM-DD");
        }else if(select == "recent_week"){
            from = moment().subtract(1, 'weeks').format("YYYY-MM-DD");
            to = moment().format("YYYY-MM-DD");
        }else if(select == "recent_month"){
            from = moment().subtract(1, 'months').format("YYYY-MM-DD");
            to = moment().format("YYYY-MM-DD");
        }

        refreshECharts(from,to);
    }

    //comfirm select date
    $("#comfirm").on('click', function(){
    	
    	from = moment($('input[name=start]').datepicker("getDate")).format("YYYY-MM-DD");
    	to = moment($('input[name=end]').datepicker("getDate")).format("YYYY-MM-DD");
    	$("#demo-default-modal").modal('hide');
        refreshECharts(from,to);
    });
});