//analyseUrlParameter
var id = AppUtils.analyseUrlParameter().id;

/**
 * Switchery checkbox
 * @type {HTMLElement | null}
 */

var enableSwitchery;
var statusSwitchery;

$(document).ready(function () {

    var enableCheckbox =document.getElementById('enable');
    enableSwitchery = new Switchery(enableCheckbox);

    var statusCheckbox =document.getElementById('status');
    statusSwitchery = new Switchery(statusCheckbox);

    select2Utils.initRemoteSelect2("department","/system/department/getDepartPage",false,"请选择部门");

    select2Utils.initRemoteSelect2("position","/system/department/getDepartPage",false,"请选择职位");


    // $('#department').bootstrapSelect({
    //     url:'/system/department/getDepartmentList',
    //     // data: {},
    //     valueField: 'id',
    //     textField: 'name',
    //     onBeforeLoad: function(target, param) {
    //
    //     },
    //     onLoadSuccess: function (target) {
    //
    //         console.log("tst")
    //     }
    // });

    if (id != undefined){
        getUserDetail(id);
    }
    // $('#department').bootstrapSelect("setValue","3");
    /**
     * checkbox change
     */
    // isEnableCheckbox.onchange = function() {
    //     if(!isEnableCheckbox.hasAttribute("checked")){
    //         $("#demo-sw").attr("checked", true);
    //     }else{
    //         $("#demo-sw").removeAttr("checked");
    //     }
    //     isEnableCheckbox.check==true?isEnableCheckbox.check=false:isEnableCheckbox.check=true;
    // };
});


//back to last page
//===============================================
function back() {
    history.back(-1);
}

//getUserDetail info
//==============================================
function getUserDetail(id) {
    $.ajax({
        url:"/system/user/getUser?id="+id,
        success:function(result){
            setUserValue(result);
        }
    });
}
//set User Info value
//==============================================
function setUserValue(user) {

    $("input[name=id]").val(user.id);
    $("input[name=username]").val(user.username);
    $("input[name=email]").val(user.email);
    $("input[name=fullname]").val(user.fullname);
    $("input[name=gender][value=" + user.gender + "]").prop("checked", true);
    $("select[name=credentialType] option[value="+user.credentialType+"]").attr("selected", "selected");
    $("input[name=credentialNum]").val(user.credentialNum);
    $("input[name=phoneNumber]").val(user.telphone);
    $("input[name=birthday]").val(user.birthday);
    $("#avatar").attr("src", user.avatar);

    if(user.enable == 0){
        SwitcheryUtils.setEnable(enableSwitchery,true)
    }else{
        SwitcheryUtils.setEnable(enableSwitchery,false)
    }

    if(user.status == 0){
        SwitcheryUtils.setEnable(statusSwitchery,true)
    }else{
        SwitcheryUtils.setEnable(statusSwitchery,false)
    }
}
