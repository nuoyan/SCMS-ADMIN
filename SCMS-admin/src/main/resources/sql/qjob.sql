#namespace("jobGroup")
  #include("qjob/qJobGroup.sql")
#end

#namespace("jobInfo")
#include("qjob/qJobInfo.sql")
#end

#namespace("jobLogGlue")
#include("qjob/qJobLogGlue.sql")
#end

#namespace("jobLog")
#include("qjob/qJobLog.sql")
#end

#namespace("jobRegistry")
#include("qjob/qJobRegistry.sql")
#end
