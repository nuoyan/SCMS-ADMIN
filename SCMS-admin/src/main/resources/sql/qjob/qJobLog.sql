#sql("triggerCountByDay")
		SELECT
			DATE_FORMAT(trigger_time,'%Y-%m-%d') triggerDay,
			COUNT(handle_code) triggerDayCount,
			SUM(CASE WHEN (trigger_code = 200 and handle_code = 0) then 1 else 0 end) as triggerDayCountRunning,
			SUM(CASE WHEN handle_code = 200 then 1 else 0 end) as triggerDayCountSuc
		FROM XXL_JOB_QRTZ_TRIGGER_LOG
		WHERE trigger_time BETWEEN '#(startDate)' and '#(endDate)'
		GROUP BY triggerDay;
#end


 