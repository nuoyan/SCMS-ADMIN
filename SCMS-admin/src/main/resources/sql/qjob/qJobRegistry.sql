#sql("findAllByTimeOut")
		SELECT *
		FROM XXL_JOB_QRTZ_TRIGGER_REGISTRY AS t
		WHERE t.update_time > DATE_ADD(NOW(),INTERVAL - #(timeout) SECOND)
#end

#sql("removeDead")
		DELETE FROM XXL_JOB_QRTZ_TRIGGER_REGISTRY
		WHERE update_time < DATE_ADD(NOW(),INTERVAL - #(timeout) SECOND)
#end

#sql("registryUpdate")
    UPDATE XXL_JOB_QRTZ_TRIGGER_REGISTRY
    SET `update_time` = NOW()
    WHERE `registry_group` = '#(registryGroup)'
    AND `registry_key` = '#(registryKey)'
    AND `registry_value` = '#(registryValue)'
#end

#sql("registrySave")
    INSERT INTO XXL_JOB_QRTZ_TRIGGER_REGISTRY( `registry_group` , `registry_key` , `registry_value`, `update_time`)
    VALUES( '#(registryGroup)', '#(registryKey)', '#(registryValue)', NOW())
#end

#sql("registryDelete")
		DELETE FROM XXL_JOB_QRTZ_TRIGGER_REGISTRY
    WHERE `registry_group` = '#(registryGroup)'
    AND `registry_key` = '#(registryKey)'
    AND `registry_value` = '#(registryValue)'
#end


 